%refer to MP3 Basics for more details on termssearch text file in folderult





function OUTPUT = Decoder(filename)
filetemp = fopen( filename, 'r', 'b');
file =  uint8( fread(filetemp, 'ubit1')');

OUTPUT = [];
load freq_inv; 
 
%Preallocating variables
frame_counter = 1;
sync_word = uint8([1 1 1 1 1 1 1 1 1 1 1 1 1 0 1]); %fixed sync word
main_data_begin = double(0); %main data offset
scfsi(1:2,1:21) = 0;

part2_3_length(1:4,1) = double(0); %size of main data
part2_length(1:4,1) = double(0);
big_values(1:4,1) = uint16(0);
bg_val_region(1:4,1) = uint16(0);
global_gain(1:4,1) =  uint8(0);
scalefac_compress(1:4,1) = uint8(0);
slen1(1:4,1) = uint8(0);
slen2(1:4,1) = uint8(0);
window_switching_flag(1:4,1) = uint8(0);
table_select(1:4,1:3) = uint8(0);
region0_count(1:4,1) = uint8(0);
region1_count(1:4,1) = uint8(0);
preflag(1:2,1) = uint8(0);
scalefac_scale(1:2,1) = uint8(0);
count1table_select(1:2,1) = uint8(0);
%for block type 2
block_type(1:4,1) = uint8(0);
mixed_block_flag(1:4,1) = uint8(0);
subblock_gain(1:4,1:3) = uint8(0);

saved_scalefacs(1:21,1:4) = uint8(0);

huffman_decoded_bits(1:4,1:576) = double(0);


i=1; %file transfer variable

while ( i + 31 <= numel(file)) %while there still exists 32 bits to process
    
    %display(i); 
    %display(file(i:i+50)); %%indicator to show which bit it's at
    if mod(i - 1,8) ~= 0
            %Find the next byte aligned value for buf_i
            temp = mod(i,8);
            temp = 8 - temp;
            i = i + 1 + temp;
    end
    
    if ( file(i:i+14) == sync_word) %checking for header sync_word
        flag=1; % flag is used throughout the program to check if the header position was correct
       
        if(file(i+15) == 0) %checking for crc protection
            crc_protection = 1;
            display('crc protected');
        else
            crc_protection = 0;
            display('NOT crc protected');
        end
        
        if (flag) %checking bitrate
            bitrate = BitrateRetrieve(bin2dec(num2str(file(i+16:i+19)))); %retrieves bitrate from frame header
            if (bitrate ~= 15) 
                %display(bitrate);
            else %bitrate index = 15 => invalid bitrate
                display('Invalid Bitrate');
                flag = 0;
            end
        end
        
        if (flag) %checking sampling freqeuncy
            sampfreq = SampFreqRetrieve(bin2dec(num2str(file(i+20:i+21)))); %retrieves sampling frequency from frame header
            if (sampfreq == 0) %bitrate index = 0 => invalid bitrate
                %display('Invalid Sampling Rate');
                flag = 0;
            else
                %display(sampfreq);
            end
        end
        
        if (flag) %checking padding information, mode, and mode extention
            info = sprintf('Decoding Frame No. %d With Bitrate %d & Sampling Frequency %2.1f kHz',frame_counter,bitrate,sampfreq);
            disp(info);
            if (file(i+22) == 1)
                padding = 1;
            else
                padding = 0;
            end
            
            mode = ModeRetrieve(bin2dec(num2str(file(i+24:i+25)))); %retrieves mode
            %disp(mode);
            modeex = ModeEXRetrieve(bin2dec(num2str(file(i+26:i+27)))); %retrieves mode extension 
            %disp(modeex);
        end
        
        if (flag) %Frame has been found successfully. Executing main processes. 
            
            cur_header_bit = i; %saving header starting bit for re-access later
            %display(dec2hex(floor(i/8)));
            %display(cur_header_bit);
            
            %determining the frame length in bytes (w/o the header bytes)
            if (crc_protection == 0)
                flb = floor(144 * bitrate / sampfreq + padding)-4;
                i=i+32; %point to side information
            else
                flb = floor(144 * bitrate / sampfreq + padding)-6;
                i=i+48; %point to side information(extra 16bits (2 bytes) for crc)
            end
            
            if (strcmp(mode,'single_channel')) %mode is single channeled, expand on this later
%                 main_data_begin = uint16(bin2dec(num2str(file(i:i+8))));
%                 %skipping private_bits
%                 temp = i+12;
%                 scfsi(1,1:4) = file(temp:temp+3);
%                 scfsi(2,1:4) = uint8(0);
%                 scfsi_share = scfsi_expand(scfsi);
%                 temp = temp+4;
%                 rowindex=1;
%                 for gr=1:2
%                         part2_3_length(rowindex) = uint16(bin2dec(num2str(file(temp:temp+11))));
%                         big_values(rowindex) = uint16(bin2dec(num2str(file(temp+12:temp+20))));
%                         global_gain(rowindex) = uint8(bin2dec(num2str(file(temp+21:temp+28))));
%                         scalefac_compress(rowindex) = uint8(bin2dec(num2str(file(temp+29:temp+32))));
%                         [slen1(rowindex), slen2(rowindex)] = scale_compress2slens(scalefac_compress(rowindex));
%                         window_switching_flag(rowindex) = file(temp+33);
%                         
%                         temp = temp + 34;
%                         
%                         if (window_switching_flag(rowindex))
%                              block_type(rowindex) = bin2dec(num2str(file(temp:temp+1)));
%                              mixed_block_flag(rowindex) = file(temp+2);
%                              temp = temp + 3;
%                              for region=1:2
%                                  table_select(rowindex,region) = bin2dec(num2str(file(temp:temp+4)));
%                                  temp = temp+5;
%                              end
%                              
%                              for windows=1:2
%                                  subblock_gain(rowindex,windows) = bin2dec(num2str(file(temp:temp+4)));
%                                  temp = temp+5;
%                              end
%                         else
%                             for region=1:3
%                                 table_select(rowindex,region) = bin2dec(num2str(file(temp:temp+4)));
%                                 temp = temp + 5;
%                             end
%                             region0_count(rowindex) = bin2dec(num2str(file(temp:temp+3)));
%                             region1_count(rowindex) = bin2dec(num2str(file(temp+4:temp+6)));
%                             temp = temp + 7;
%                         end
%                         preflag(rowindex) = file(temp);
%                         scalefac_scale(rowindex) = file(temp+1);
%                         count1table_select = file(temp+2);
%                         temp = temp +3;
%                         rowindex = rowindex + 1;
%                 end
            
            else %mode is dual-channeled
                
                main_data_begin = double(bin2dec(num2str(file(i:i+8)))); %pointer that provides offset to beginning of main data. The value here determines how many bits to move back from header bit.
                %skipping private_bits(3 bits)
                %temp = i+12;  
                scfsi(1,1:4) = file(i+12:i+15); %determining whether scalefactors will be shared . Channel 1.
                scfsi(2,1:4) = file(i+16:i+19); % same as above. channel 2.
                scfsi_share = scfsi_expand(scfsi); % expand the values so from 4 bits to 21 bits (since 21 bands are being used).
               
                temp = i + 20; %another pointer initialized for easier organization
                rowindex=1; % rowindex defines which set of values we are dealing with. 1 = Granule1Channel1. 2 = Granule1Channel2. etc.
                for gr=1:2 %decoding information that will be used in scalfactor and huffman decoding.
                    for ch=1:2
                        part2_3_length(rowindex) = uint16(bin2dec(num2str(file(temp:temp+11))));
                        big_values(rowindex) = uint16(bin2dec(num2str(file(temp+12:temp+20))));
                        global_gain(rowindex) = uint8(bin2dec(num2str(file(temp+21:temp+28))));
                        scalefac_compress(rowindex) = uint8(bin2dec(num2str(file(temp+29:temp+32))));
                        [slen1(rowindex), slen2(rowindex)] = scale_compress2slens(scalefac_compress(rowindex));
                        window_switching_flag(rowindex) = uint8(file(temp+33));
                        
                        temp = temp+34;
                        
                        if (window_switching_flag(rowindex))
                            block_type(rowindex) = uint8(bin2dec(num2str(file(temp:temp+1))));
                            mixed_block_flag(rowindex) = uint8(file(temp+2));
                            temp = temp + 3;
                            for region=1:2
                                table_select(rowindex,region) = uint8(bin2dec(num2str(file(temp:temp+4))));
                                temp=temp+5;
                            end
                            
                            for windows=1:3
                                subblock_gain(rowindex,windows) = uint8(bin2dec(num2str(file(temp:temp+2))));
                                temp=temp+3;
                            end
                            
                            %if( block_type(ri) == 1 ||  block_type(ri) == 3 )
                            %        part2_length(ri) = 11*slen1(ri) + 10*slen2(ri);
                            %        region_address1(ri) = uint8( 8 );
                        else
                            for region=1:3
                                table_select(rowindex,region) = bin2dec(num2str(file(temp:temp+4)));
                                temp = temp + 5;
                            end
                            
                            region0_count(rowindex) = bin2dec(num2str(file(temp:temp+3)));
                            region1_count(rowindex) = bin2dec(num2str(file(temp+4:temp+6)));
                            temp = temp + 7;
                            
                            block_type(rowindex) = uint8(0);
                            
                        end
                        
                        preflag(rowindex) = file(temp);
                        scalefac_scale(rowindex) = file(temp+1);
                        count1table_select(rowindex) = file(temp+2);
                        temp = temp +3;
                        rowindex = rowindex + 1;
                    end
                end %End of extracting side info
           
                             
                
                %moving to decode main data
                where_side_ends = temp - 1;
                %display(where_side_ends);
                if (main_data_begin == 0) %main data follows right after side
                    i = temp;
                    next_header = cur_header_bit + ((flb * 8) + 32 + (crc_protection * 16)) - 1;
                    %display('next frame plz');
                else
                    %info4frame = sprintf('Destructive Decoding Required For Frame # %d',frame_counter);
                    %   disp(info4frame);
                    %   info4frame = sprintf('No. Of Bytes In Previous Frame: %d', main_data_begin);
                    %   disp(info4frame);
                    i = cur_header_bit - (8*main_data_begin);
                    
                    %display(i);
                    file(cur_header_bit:where_side_ends) = []; %remove header and side info
                    next_header = cur_header_bit + ((flb * 8) - (32 * 8)) - 1; %Attempting to find next header. sub
                end
                %display(next_header);
                
                %display(class(i));
                
                %Start Huffman Decoding & Requnatization
                rowindex = 1;
                %if (strcmp(mode,'single_channel'))  %ignore this for now.
                %Can expand on single channel when we have time
                %else
                for gr=1:2
                    for ch=1:2

                        %display(i);
                       % display(part2_3_length(rowindex));
                        if (part2_3_length == 0)
                            %no main data to decode. skip
                        else
                            %Determining some information for
                            %allocation
                            main_data_end = i + part2_3_length(rowindex) - 1;
                            %scflength = scflength_select( block_type(rowindex), mixed_block_flag(rowindex), slen1(rowindex), slen2(rowindex) );   

                            if (block_type(rowindex) == 0 || block_type(rowindex) == 1 || block_type(rowindex) == 3)
                                part2_length(rowindex) = 11* slen1(rowindex) + 10*slen2(rowindex);
                                if ( window_switching_flag(rowindex))
                                region0_count(rowindex) = 7;
                                 region1_count(rowindex) = 13;
                                end
                            elseif (block_type(rowindex) == 2 && mixed_block_flag(rowindex)==1)
                                part2_length(rowindex) = 17* slen1(rowindex) + 18*slen2(rowindex);
                                region0_count(rowindex) = 8;
                                region1_count(rowindex) = 0;
                            elseif (block_type(rowindex) == 2 && mixed_block_flag(rowindex)==0)
                                part2_length(rowindex) = 18* slen1(rowindex) + 18*slen2(rowindex);
                                region0_count(rowindex) = 8;
                                region1_count(rowindex) = 0;
                            end

                               if (frame_counter == 5)
                                %display(i);
                            %rrr = slen1;
                            %xxx = i;
                            end
                            ib4scal = i;
                            %display(ib4scal);
                            %Decode Scalefactor
                            %display(slen1(rowindex));
                            %display(slen2(rowindex));
                            %display(part2_length(rowindex));
                            [scalefactors, save_accessed] = get_scalefactors(file(i:i+part2_length(rowindex)-1), window_switching_flag(rowindex), block_type(rowindex), mixed_block_flag(rowindex), slen1(rowindex), slen2(rowindex), scfsi_share,ch , gr, saved_scalefacs);
                            if (gr == 1)
                                         if (scfsi_share(ch,1) ~= 0 || scfsi_share(ch,7) ~= 0 || scfsi_share(ch,12) ~= 0 || scfsi_share(ch,17) ~= 0)
                                             %SAVE SCALEFACTORS OF THIS CHANNEL
                                             %display(scfsi_share);
                                             saved_scalefacs(:,ch) = scalefactors(:,1);
                                         end
                            end 
                            %display(scalefac);
                            if (multi_if(save_accessed)==1 )
                                for bands=1:11
                                    if (save_accessed(bands))
                                        i = i + double(slen1(rowindex));
                                    end
                                end
                                for bands = 12:21
                                    if (save_accessed(bands))
                                        i = i + double(slen2(rowindex));
                                    end
                                end
                               
                         
                            else
                                %display(i+part2_length(rowindex));
                                i = i + part2_length(rowindex);
                            end
                            
                            %display(i);
                            
                            
                            %display(i);
                            %%display(numel(x(i:channel_main_data_end)));

                            %%Huffman deCoding
                            [short_cbs, long_cbs] = get_cbrange(sampfreq); %determining the critical band ranges
                            bg_val_region(rowindex) = 2 * big_values(rowindex);
                            %display(i);
                            %display(class(i));
                            
                            %display(region0_count);
                            %display(region1_count);
                            huffman_decoded_bits(rowindex,:) = huffman_decode(file(i:end),numel(file(i:main_data_end)),bg_val_region(rowindex),region0_count(rowindex),region1_count(rowindex),table_select(rowindex,:),count1table_select(rowindex),block_type(rowindex),window_switching_flag(rowindex),long_cbs);
                            %display(huffman_decoded_bits(rowindex,1:576));
                            %display(block_type(rowindex)); 
                            %display(huffman_decoded_bits(rowindex,1:50));

                            i = main_data_end + 1;
                            
                            if (frame_counter == 7)
                                b4requa(rowindex,1:576) = huffman_decoded_bits(rowindex,:);
                                
                            end


                            %REQUANTIZATION. YET TO BE COMPLETED.PLEASE
                            %JUST USE  THE OUTPUT VARIABLE FOR NOW.
                            
                            huffman_decoded_bits(rowindex,:) = requantizer(huffman_decoded_bits(rowindex,:),scalefactors,short_cbs,long_cbs ,block_type(rowindex),window_switching_flag(rowindex), global_gain(rowindex), subblock_gain(rowindex,:), preflag(rowindex), scalefac_scale(rowindex), mixed_block_flag(rowindex));
                            
                            if window_switching_flag(rowindex) && block_type(rowindex)==2
                                   huffman_decoded_bits(rowindex,:) = reordering(huffman_decoded_bits(rowindex,:), short_cbs,long_cbs, mixed_block_flag(rowindex));
                            end  
                            

                            %%reordering process if block 2 and
                            %%window_switching

                            %reset scalefactors for next iteration use
                            scalefactors(:,:) = 0;
                            save_accessed = 0;
                        end
                        rowindex = rowindex + 1;
                    end
                end %end of decoding main data. Moving on to convert processes
                    
                %IMDCT
                temp_PCM_samples     = IMDCT(huffman_decoded_bits , block_type, window_switching_flag,mixed_block_flag);
                temp_PCM_samples     = temp_PCM_samples .* freq_inv;
                
                temp_PCM_samples     = f(temp_PCM_samples);  
                
                
                if isempty(OUTPUT)
                        OUTPUT(1,1:1152) = [temp_PCM_samples(1,:) temp_PCM_samples(3,:)]; %channel 1
                        OUTPUT(2,1:1152) = [temp_PCM_samples(2,:) temp_PCM_samples(4,:)]; %channel 2
                else
                    length_PCM = numel(OUTPUT(1,:));
                    OUTPUT(1,length_PCM+1:length_PCM+1152) = [temp_PCM_samples(1,:) temp_PCM_samples(3,:)]; %channel 1
                    OUTPUT(2,length_PCM+1:length_PCM+1152) = [temp_PCM_samples(2,:) temp_PCM_samples(4,:)]; %channel 2
                end
                
                %Frequency inversion Using Loaded Variable freq_inv

                %filterbank process

                %convert the the two granules to left, right speakers
                %(preparation-for-output process )

                %clear variables for next frame
                scfsi(:,:) = 0;
                huffman_decoded_bits(:,:) = 0;
                saved_scalefacs(:,:) = 0;
                frame_counter = frame_counter+1;
            end
            %display(next_header);
            %display(flb);
        i = next_header;
        end
        info = sprintf('Done decodeing Frame #%d',frame_counter - 1);
        display(info);
        
    end
    i = i+1;
end
display('All frames have been decoded.');
end
    
