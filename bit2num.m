function number = bit2num(stream)
if (numel(stream) == 0)
    number = 0;
else
    number = bin2dec(num2str(stream));
end
end