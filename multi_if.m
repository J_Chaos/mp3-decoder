function boolean = multi_if (array)
    boolean = 0;
    for i=1:numel(array)
        if array(i) == 1
            boolean = 1;
        end
    end
end