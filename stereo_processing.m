function output_data = stereo_processing (huffman_bits,intensity_stereo,ms_stereo)

    if ms_stereo
        output(1,:) = (huffman_bits(1,:) + huffman_bits(2,:)) / sqrt(2); %Left Channel in Granule 1
        output(2,:) = (huffman_bits(1,:) - huffman_bits(2,:)) / sqrt(2); %Right Channel in Granule 1
        output(3,:) = (huffman_bits(3,:) + huffman_bits(4,:)) / sqrt(2); %Left Channel in Granule 2
        output(4,:) = (huffman_bits(3,:) - huffman_bits(4,:)) / sqrt(2); %Right Channel in Granule 2
    end
    
    if intensity_stereo
        %ignored due to rare usage in modern standards
end
end
        