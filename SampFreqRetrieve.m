function SampFreq = SampFreqRetrieve(index)
% This function decodes the SampRate information and returns the
% corresponding SampRate


SAMPLINGRATELUT = [44.1 48.0 32.0 0];

SampFreq = SAMPLINGRATELUT(1,index+1);
end
    