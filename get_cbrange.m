function [short_cbs, long_cbs]  = get_cbrange (frequency)
    long_cbs(1:23) = 0;
    short_cbs(1:14) = 0;
    if (frequency == 32)
        short_cbs = [0,4,8,12,16,22,30,42,58,78,104,138,180,192];
        long_cbs = [0,4,8,12,16,20,24,30,36,44,54,66,82,102,126,156,194,240,296,364,448,550,576];
    elseif (frequency == 44.1)
        short_cbs = [0,4,8,12,16,22,30,40,52,66,84,106,136,192];
        long_cbs = [0,4,8,12,16,20,24,30,36,44,52,62,74,90,110,134,162,196,238,288,342,418,576];
    elseif (frequency == 48)
        short_cbs = [0,4,8,12,16,22,28,38,50,64,80,100,126,192];
        long_cbs = [0,4,8,12,16,20,24,30,36,42,50,60,72,88,106,128,156,190,230,276,330,384,576];
    end
end
        
