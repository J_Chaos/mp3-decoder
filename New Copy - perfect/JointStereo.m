function OUTPUT = JointStereo (HUFFMAN_BITS,IS,MSS)

    %MS_STEREO
    if MSS
        OUTPUT(1,:) = (HUFFMAN_BITS(1,:) + HUFFMAN_BITS(2,:)) / sqrt(2); 
        OUTPUT(2,:) = (HUFFMAN_BITS(1,:) - HUFFMAN_BITS(2,:)) / sqrt(2); 
        OUTPUT(3,:) = (HUFFMAN_BITS(3,:) + HUFFMAN_BITS(4,:)) / sqrt(2); 
        OUTPUT(4,:) = (HUFFMAN_BITS(3,:) - HUFFMAN_BITS(4,:)) / sqrt(2); 
    end
    
    %INTENSITY_STEREO
    if IS
        %NOT USED MUCH, IGNORED;
	end
end
        