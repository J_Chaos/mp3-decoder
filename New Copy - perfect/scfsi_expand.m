function scfsi_band = scfsi_expand(share)
    scfsi_band = uint8(zeros(2,21));
    for ch=1:2
        scfsi_band(ch,1:6) = share(ch,1);
        scfsi_band(ch,7:11) = share (ch,2);
        scfsi_band(ch,12:16) = share (ch,3);
        scfsi_band(ch,17:21) = share (ch,4);
    end
end