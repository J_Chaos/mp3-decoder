%%REQUANTIZER
%%this function receives a huffman decoded set of 1:576 frequency lines 
%%and requantizes the values. Short blocks (block_type = 2) are handled
%%differently than long blocks (block_type = 0,1,3).
%%a more detailed process can be viewed in the documentation's reference


function requantized_vals = requantizer(huffman_decoded_bits, scale_factors, short_cbs,long_cbs, block_type,window_switching_flag, global_gain, subblock_gain, preflag, scalefac_scale, mixed_block_flag)

    requantized_vals (1:576) = 0;
    pretab = double ([0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 2 2 3 3 3 2 0]);

    scale_factors = double (scale_factors);
    global_gain = double (global_gain);
    subblock_gain = double (subblock_gain);
    preflag = double (preflag);

    cur_cb = 1;
    cur_freq = 1;


    if (scalefac_scale == 0)
        scalefac_multiplier = 0.5;
    else
        scalefac_multiplier = 1.0;
    end

    if (window_switching_flag && block_type == 2)
        if (mixed_block_flag) %do 8 long window requantizations first
            C = 1/4 * (global_gain - 210);
            while (cur_cb <= 8)
                while ( cur_freq>=(long_cbs(cur_cb)+1) && cur_freq<(long_cbs(cur_cb+1)+1)) 
                D = -(scalefac_multiplier * (scale_factors(cb, 1) + preflag * pretab(cb)));
                requantized_vals(cur_freq) = sign(huffman_decoded_bits(cur_freq)) * (abs(huffman_decoded_bits(cur_freq)))^(4/3) * (2^C) * (2^D);
                cur_freq = cur_freq + 1;
                end
                cur_cb = cur_cb + 1;
            end           
            cur_cb = 4;
            while (cur_cb<=12)
                cb_length = ((short_cbs(cur_cb+1) - short_cbs(cur_cb)) * 3);
                window_length = cb_length / 3;
                %while (cur_freq>=((short_cbs(cur_cb)*3)+1) && cur_freq<(cb_length+short_cbs(cur_cb) + 1))
                    for window=1:3
                        A = 1/4 * (global_gain - 210 - 8 * subblock_gain(window));
                        for freqsinwindows = 1 : window_length
                            B = -(scalefac_multiplier * scale_factors(cur_cb,window+1)); %+1 since first spot is used for long blocks
                            requantized_vals(cur_freq) = sign(huffman_decoded_bits(cur_freq)) * (abs(huffman_decoded_bits(cur_freq)))^(4/3) * (2^A) * (2^B);
                            cur_freq = cur_freq + 1;
                        end
                    end
                %end
                cur_cb = cur_cb + 1;
            end
                        
        else
           
        while (cur_cb <=12)
            cb_length = ((short_cbs(cur_cb+1) - short_cbs(cur_cb)) * 3);
            window_length = cb_length/3;
            for window=1:3
                A = 1/4 * (global_gain - 210 - 8 * subblock_gain(window));
                for freqsinwindows = 1:window_length
                    B = -( scalefac_multiplier * scale_factors(cur_cb,window+1));
                    requantized_vals(cur_freq) = sign(huffman_decoded_bits(cur_freq)) * (abs(huffman_decoded_bits(cur_freq)))^(4/3) * (2^A) * (2^B);
                    cur_freq = cur_freq + 1;
                end
            end
            cur_cb = cur_cb + 1;
        end
        end
    else
        C = 1/4 * (global_gain - 210);
        while (cur_cb <= 21)
            while ( cur_freq>=(long_cbs(cur_cb)+1) && cur_freq<(long_cbs(cur_cb+1)+1)) %%while curr_freq resides in the first subband
                D = -(scalefac_multiplier * (scale_factors(cur_cb, 1) + preflag * pretab(cur_cb)));
                requantized_vals(cur_freq) = sign(huffman_decoded_bits(cur_freq)) * (abs(huffman_decoded_bits(cur_freq)))^(4/3) * (2^C) * (2^D);
                cur_freq = cur_freq + 1;
            end
            cur_cb = cur_cb + 1;
        end
    end
end
        
    
    