%this function converts te scale_compress value to the corresponding slen1, slen2
function [slen1,slen2] = scale_compress2slens(scalefac_compress)

SCALEFAC_COMPRESSLUT = [0 0; 0 1; 0 2; 0 3; 3 0; 1 1; 1 2; 1 3; 2 1; 2 2; 2 3; 3 1; 3 2; 3 3; 4 2; 4 3;];

slen1 = SCALEFAC_COMPRESSLUT(scalefac_compress+1 , 1);
slen2 = SCALEFAC_COMPRESSLUT(scalefac_compress+1 , 2);