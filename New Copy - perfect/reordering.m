function OUTPUT = reordering(INPUT, SHORT_CBS, MIXED_BLOCK_FLAG)
    OUTPUT(1:576) = 0;
    if MIXED_BLOCK_FLAG
        OUTPUT(1:36) = INPUT(1:36);
        CB = 9;
        Max = 17;
    else
        CB = 1;
        Max = 12;
    end
    while CB<=Max
        TEMP = INPUT((SHORT_CBS(CB)*3)+1:(SHORT_CBS(CB+1))*3);
        CB_Length = numel(TEMP);
        CB_Length_Window = CB_Length/3;
        ReorderedTEMP = zeros (1, CB_Length);
        Reordered = 0;
        k = 1;
        while Reordered ~= CB_Length
            i = k:CB_Length_Window:CB_Length;
            ReorderedTEMP( Reordered+1 : Reordered+3)= TEMP(i);
            k = k+1;
            Reordered = Reordered + 3;
        end
        OUTPUT((SHORT_CBS(CB)*3)+1:(SHORT_CBS(CB+1))*3) = ReorderedTEMP;
        CB = CB+1;
    end 
end