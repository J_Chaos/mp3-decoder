function mode = ModeRetrieve(index)

if (index == 0)
mode = 'stereo';
elseif (index == 1)
    mode = 'joint_stereo';
elseif (index == 2)
    mode = 'dual_channel';
else
    mode = 'single_channel';
end