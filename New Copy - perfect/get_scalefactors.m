%this function receives a size of part2_length and decodes the scale factors out of it.
function [scalefactor save_accessed]  = get_scalefactors(i_bitstream, window_switching_flag, block_type, mixed_block_flag, slen1, slen2, scfsi_share, ch, gr, saved_scalefac)
    i=double(1);
    save_accessed(1:21) = 0;
    if ( window_switching_flag && block_type== 2 )
        scalefactor(21,4) = 0; %initializing
        
        if (~mixed_block_flag) %%short only
            
            for bands = 1:6
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen1-1)));
                    i = i + slen1;
                end
            end
            
            for bands = 7:12
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen2-1)));
                    i = i + slen2;
                end
            end
            
            
            
        else
            
            
            
            for bands = 1:8 %slen1 transferred to long blocks here
                scalefactor(bands,1) = uint8(bit2num(i_bitstream(i:i+slen1-1)));
                i = i + slen1;
            end
            
            for bands = 4:6 %slen1 transferred to short blocks here
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen1-1)));
                    i = i+slen1;
                end
            end
            
            for bands = 7:12 %slen2 transferred to short blocks here
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen2-1)));
                    i = i + slen2;
                end
            end
            
            
        end
    else
        scalefactor(21,4) = 0; %initializing
        for bands=1:21 %%fixed # of bands
            if( scfsi_share(ch,bands) == 0 || gr == 1 )
                if (bands >=1 && bands <=11)

                    scalefactor(bands,1) = uint8( bit2num(i_bitstream(i : i+slen1-1) ));

                    i = i + double(slen1);
                
                else 
                    scalefactor(bands,1) = uint8( bit2num (i_bitstream(i : i+slen2-1) ));
                    i = i + double(slen2);
                end

                
            else
                scalefactor(bands,1) = saved_scalefac(bands,ch);
                save_accessed(bands) = 1;
                
            end
        end

    end

end
    