function OUTPUT = IMDCT(INPUT, BLOCKTYPE, MIXEDBLOCKFLAG, WINDOWFLAG)
global COSINELONG COSINESHORT SIN_0 SIN_1 SIN_2 SIN_3;;
OUTPUT  = zeros(4,576);
persistent ADDER;
OUTPUTTEMP(1,36) = double(0);
BLOCK2TEMP(1:3,1:12) = 0;

if isempty(ADDER)
    ADDER = zeros(2,576);
end

for loop = 1:4
    if(mod(loop,2) == 1)
        Ch = 1;
    else
        Ch = 2;
    end
    
    
    if BLOCKTYPE(loop) == 2 && MIXEDBLOCKFLAG(loop)
        i = 1;
        if WINDOWFLAG(loop)
            while i <= 36
                TEMP = INPUT(loop, i:i+17);
                OUTPUTTEMP(:) = 0;
                if(all(TEMP == 0))
                else
                    for x = 1:36
                        OUTPUTTEMP(x) = sum(TEMP .* COSINELONG(x,:));
                    end
                    OUTPUTTEMP = OUTPUTTEMP .* SIN_0;
                end
                OUTPUT(loop,i:i+17) = OUTPUTTEMP(1:18)+ADDER(Ch,i:i+17);
                ADDER(Ch, i:i+17) = OUTPUTTEMP(19:36);
                
                i = i+ 18;
            end
        end
        while i <= 576
            TEMP = INPUT(loop, i:i+17);
            BLOCK2TEMP(:,:) = 0;
            OUTPUTTEMP(:) = 0;
            if(all(TEMP == 0))
            else
                for w = 1:3
                    WORK = TEMP(w:3:18);
                    for x = 1:12
                            BLOCK2TEMP(w,x) = sum(WORK .* COSINESHORT(x,:));
                    end
                end
                
                for w = 1:3
                     BLOCK2TEMP(w,:) = BLOCK2TEMP(w,:) .* SIN_2;
                end
                
                OUTPUTTEMP(1:6)   = 0;
                OUTPUTTEMP(7:12)  = BLOCK2TEMP(1, 1:6);
                OUTPUTTEMP(13:18) = BLOCK2TEMP(1, 7:12) + BLOCK2TEMP(2, 1:6);
                OUTPUTTEMP(19:24) = BLOCK2TEMP(2, 7:12) + BLOCK2TEMP(3, 1:6);
                OUTPUTTEMP(25:30) = BLOCK2TEMP(3, 7:12);
                OUTPUTTEMP(31:36) = 0;
            end
            

            OUTPUT(loop,i:i+17) = OUTPUTTEMP(1:18)+ADDER(Ch,i:i+17);
            ADDER(Ch, i:i+17) = OUTPUTTEMP(19:36);
            
            i = i+ 18;
        end
    else
        i = 1;
        while i<= 576
            TEMP = INPUT(loop, i:i+17);
            OUTPUTTEMP(:) = 0;
            if(all(TEMP == 0))
            else
                for m = 1:36
                    OUTPUTTEMP(m) = sum(TEMP .* COSINELONG(m,:));
                end
                
                if BLOCKTYPE(loop) == 0
                    OUTPUTTEMP = OUTPUTTEMP .* SIN_0; %sin(pi/36*((x-1)+1/2));
                elseif BLOCKTYPE(loop) == 1
                        OUTPUTTEMP = OUTPUTTEMP .* SIN_1; %sin(pi/36*((x-1)+1/2));
                elseif BLOCKTYPE(loop) == 3
                        OUTPUTTEMP = OUTPUTTEMP .* SIN_3; %sin(pi/12*((x-1)-6+1/2));
                end
            end
            OUTPUT(loop,i:i+17) = OUTPUTTEMP(1:18)+ADDER(Ch,i:i+17);
            ADDER(Ch, i:i+17) = OUTPUTTEMP(19:36);
            i = i+18;
        end
    end
end