function OUTPUT = Filterbank (INPUT)

global COSINE D;
OUTPUT(1:4,1:576) = double(0);
persistent FIFO;
if isempty (FIFO)
    FIFO = zeros(2,1024);
end

u(1:512) = double(0);
TEMPHOLDER = zeros(1,32);

for iter = 1:4
    if mod(iter,2) == 1;
        CH = 1;
    else
        CH = 2;
    end
    
    m = 1;
    for x = 1:18
        temp = INPUT(iter,m:18:576);
        FIFO(CH, 65:1024) = FIFO(CH, 1:960);
        FIFO(CH, 1:64) = 0;
        
        for j = 1:64
            FIFO(CH, j) = sum( temp .* COSINE(j,:));
        end

        for i = 0:7
            for j = 0:31
                u( i*64 + j  + 1) = FIFO(CH, i*128 + j  + 1);
                u( i*64 + 33 + j) = FIFO(CH, i*128 + 97 + j);
            end
        end
        
        
        w = u .* D;
        
        for j = 0:31
            SUM = 0;
            for i = 0:15
                SUM = SUM + w(j + 32 * i + 1);
            end
            TEMPHOLDER(j+1) = SUM;
        end
        
        OI = m * 32;
        OUTPUT(iter, OI - 31:OI ) = TEMPHOLDER;
        m = m + 1;
    end
end
end