function OUTPUT = IMDCT(input_data, block_type,switch_point, window_switching_flag)
OUTPUT  = zeros(4,576);
persistent ADDER;
OUTPUTTEMP(1,36) = double(0);
BLOCK2TEMP(1:3,1:12) = 0;

if isempty(ADDER)
    ADDER = zeros(2,576);
end


for i = 0:35
    for k = 0:17
        COSINELONG(i+1,k+1) = cos( (2*i+19) * (2*k+1) * (pi/72));
    end
end

%COS FUNCTION FOR IMDCT OF SHORT BLOCKS.
for i = 0:11
    for k = 0:5
        COSINESHORT(i+1,k+1) = cos (pi/24 * (2*i + 7) * (2*k + 1));
    end
end

%IMDCT WINDOWING FUNCTION FOR LONG BLOCKS (BLOCK_TYPE = 0)
temp = 0:35;
sin_window_0(temp+1) = sin(pi/36 * (temp + 0.5));

%IMDCT WINDOWING FUNCTION FOR LONG BLOCKS (BLOCK_TYPE = 1)
temp = 0:17;
sin_window_1(temp+1) = sin(pi/36 * (temp + 0.5));
sin_window_1(19:24) = 1;
temp = 24:29;
sin_window_1(temp+1) = sin(pi/12 * (temp - 17.5));
sin_window_1(31:36) = 0;

%IMDCT WINDOWING FUNCTION FOR SHORT BLOCKS (BLOCK_TYPE = 2)
temp = 0:11;
sin_window_2(temp+1) = sin(pi/12 * (temp + 0.5));

%IMDCT WINDOWING FUNCTION FOR LONG BLOCKS (BLOCK_TYPE = 3)
sin_window_3(1:6) = 0;
temp = 6:11;
sin_window_3(temp+1) = sin(pi/12 * (temp - 5.5));
sin_window_3(13:18) = 1;
temp = 18:35;
sin_window_3(temp+1) = sin(pi/36 * (temp + 0.5));

for loop = 1:4
    if(mod(loop,2) == 1)
        Ch = 1;
    else
        Ch = 2;
    end
    
    
    
    if block_type(loop) ~= 2 && window_switching_flag(loop) ~=1
        i = 1;
        while i<= 576
            TEMP = input_data(loop, i:i+17);
            OUTPUTTEMP(:) = 0;
            if(all(TEMP == 0))
            else
                for x = 1:36
                    OUTPUTTEMP(x) = sum(TEMP .* COSINELONG(x,:));
                end
                
                if block_type(loop) == 0
                    OUTPUTTEMP = OUTPUTTEMP .* sin_window_0; %sin(pi/36*((x-1)+1/2));
                elseif block_type(loop) == 1
                        OUTPUTTEMP = OUTPUTTEMP .* sin_window_1; %sin(pi/36*((x-1)+1/2));
                elseif block_type(loop) == 3
                        OUTPUTTEMP = OUTPUTTEMP .* sin_window_3; %sin(pi/12*((x-1)-6+1/2));
                end
            end
            OUTPUT(loop,i:i+17) = OUTPUTTEMP(1:18)+ADDER(Ch,i:i+17);
            ADDER(Ch, i:i+17) = OUTPUTTEMP(19:36);
            i = i+18;
        end
    else
        i = 1;
        if switch_point(loop)
            while i <= 36
                TEMP = input_data(loop, i:i+17);
                OUTPUTTEMP(:) = 0;
                if(all(TEMP == 0))
                else
                    for x = 1:36
                        OUTPUTTEMP(x) = sum(TEMP .* COSINELONG(x,:));
                    end
                    OUTPUTTEMP = OUTPUTTEMP .* sin_window_0;
                end
                OUTPUT(loop,i:i+17) = OUTPUTTEMP(1:18)+ADDER(Ch,i:i+17);
                ADDER(Ch, i:i+17) = OUTPUTTEMP(19:36);
                
                i = i+ 18;
            end
        end
        while i <= 576
            TEMP = input_data(loop, i:i+17);
            BLOCK2TEMP(:,:) = 0;
            OUTPUTTEMP(:) = 0;
            if(all(TEMP == 0))
            else
                for w = 1:3
                    WORK = TEMP(w:3:18);
                    for x = 1:12
                            BLOCK2TEMP(w,x) = sum(WORK .* COSINESHORT(x,:));
                    end
                end
                
                for w = 1:3
                     BLOCK2TEMP(w,:) = BLOCK2TEMP(w,:) .* sin_window_2;
                end
                
                OUTPUTTEMP(1:6)   = 0;
                OUTPUTTEMP(7:12)  = BLOCK2TEMP(1, 1:6);
                OUTPUTTEMP(13:18) = BLOCK2TEMP(1, 7:12) + BLOCK2TEMP(2, 1:6);
                OUTPUTTEMP(19:24) = BLOCK2TEMP(2, 7:12) + BLOCK2TEMP(3, 1:6);
                OUTPUTTEMP(25:30) = BLOCK2TEMP(3, 7:12);
                OUTPUTTEMP(31:36) = 0;
            end
            

            OUTPUT(loop,i:i+17) = OUTPUTTEMP(1:18)+ADDER(Ch,i:i+17);
            ADDER(Ch, i:i+17) = OUTPUTTEMP(19:36);
            
            i = i+ 18;
        end
    end
end