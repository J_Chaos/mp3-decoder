function varargout = ReadFile(varargin)
% READFILE MATLAB code for ReadFile.fig
%      READFILE, by itself, creates a new READFILE or raises the existing
%      singleton*.
%
%      H = READFILE returns the handle to a new READFILE or the handle to
%      the existing singleton*.
%
%      READFILE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in READFILE.M with the given input arguments.
%
%      READFILE('Property','Value',...) creates a new READFILE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ReadFile_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ReadFile_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ReadFile

% Last Modified by GUIDE v2.5 06-Apr-2013 22:23:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ReadFile_OpeningFcn, ...
                   'gui_OutputFcn',  @ReadFile_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ReadFile is made visible.
function ReadFile_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ReadFile (see VARARGIN)

% Choose default command line output for ReadFile
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ReadFile wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ReadFile_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename pathname] = uigetfile({'*.txt'},'File Selector');
fullpathname = strcat(pathname,filename);
text = fileread(fullpathname);
set(handles.text2, 'String', fullpathname); %shows path name
set(handles.text3, 'String', text); %shows info


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outPCMs = Decoder(fullpathname);
sound(outPCMs,44100);
