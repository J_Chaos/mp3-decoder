
function [OUTPUT,FREQ]= Decoder(filename)
filetemp = fopen( filename, 'r', 'b');
file =  uint8( fread(filetemp, 'ubit1')');
OUTPUT = [];
load freq_inv;

frame_counter = 1;
sync_word = uint8([1 1 1 1 1 1 1 1 1 1 1 1 1 0 1]);
main_data_begin = double(0);
scfsi(1:2,1:21) = 0;
part2_3_length(1:4,1) = double(0);
part2_length(1:4,1) = double(0);
big_values(1:4,1) = uint16(0);
bg_val_region(1:4,1) = uint16(0);
global_gain(1:4,1) =  uint8(0);
scalefac_compress(1:4,1) = uint8(0);
slen1(1:4,1) = uint8(0);
slen2(1:4,1) = uint8(0);
window_switching_flag(1:4,1) = uint8(0);
table_select(1:4,1:3) = uint8(0);
region0_count(1:4,1) = uint8(0);
region1_count(1:4,1) = uint8(0);
preflag(1:2,1) = uint8(0);
scalefac_scale(1:2,1) = uint8(0);
count1table_select(1:2,1) = uint8(0);
block_type(1:4,1) = uint8(0);
mixed_block_flag(1:4,1) = uint8(0);
subblock_gain(1:4,1:3) = uint8(0);
saved_scalefacs(1:21,1:4) = uint8(0);
huffman_decoded_bits(1:4,1:576) = double(0);


i=1;
while ( i + 31 <= numel(file))
    
    
    
    if mod(i - 1,8) ~= 0
        
        temp = mod(i,8);
        temp = 8 - temp;
        i = i + 1 + temp;
    end
    
    if ( file(i:i+14) == sync_word)
        flag=1;
        
        if(file(i+15) == 0)
            crc_protection = 1;
            
        else
            crc_protection = 0;
            
        end
        
        if (flag)
            bitrate = BitrateRetrieve(bin2dec(num2str(file(i+16:i+19))));
            if (bitrate ~= 15)
                
            else
                
                flag = 0;
            end
        end
        
        if (flag)
            FREQ = SampFreqRetrieve(bin2dec(num2str(file(i+20:i+21))));
            if (FREQ == 0)
                
                flag = 0;
            else
                
            end
        end
        
        if (flag)
            if (file(i+22) == 1)
                padding = 1;
            else
                padding = 0;
            end
            
            mode = ModeRetrieve(bin2dec(num2str(file(i+24:i+25))));
            
            modeex = bin2dec(num2str(file(i+26:i+27)));
            
            if (modeex == 0)
                intensity_stereo = uint8(0);
                ms_stereo = uint8(1);
            elseif (modeex == 1)
                intensity_stereo = uint8(1);
                ms_stereo = uint8(0);
            elseif (modeex == 2)
                intensity_stereo = uint8(0);
                ms_stereo = uint8(1);
            elseif (modeex == 3)
                intensity_stereo = uint8(1);
                ms_stereo = uint8(1);
            end
           
        end
        
        if (flag)
            
            cur_header_bit = i;
            
            
            
            
            if (crc_protection == 0)
                flb = floor(144 * bitrate / FREQ + padding)-4;
                i=i+32;
            else
                flb = floor(144 * bitrate / FREQ + padding)-6;
                i=i+48;
            end
            
            if (strcmp(mode,'single_channel'))
                
            else
                
                main_data_begin = double(bin2dec(num2str(file(i:i+8))));
                
                
                scfsi(1,1:4) = file(i+12:i+15);
                scfsi(2,1:4) = file(i+16:i+19);
                scfsi_share = scfsi_expand(scfsi);
                
                temp = i + 20;
                rowindex=1;
                for gr=1:2
                    for ch=1:2
                        part2_3_length(rowindex) = uint16(bin2dec(num2str(file(temp:temp+11))));
                        big_values(rowindex) = uint16(bin2dec(num2str(file(temp+12:temp+20))));
                        global_gain(rowindex) = uint8(bin2dec(num2str(file(temp+21:temp+28))));
                        scalefac_compress(rowindex) = uint8(bin2dec(num2str(file(temp+29:temp+32))));
                        [slen1(rowindex), slen2(rowindex)] = scale_compress2slens(scalefac_compress(rowindex));
                        window_switching_flag(rowindex) = uint8(file(temp+33));
                        
                        temp = temp+34;
                        
                        if (window_switching_flag(rowindex))
                            block_type(rowindex) = uint8(bin2dec(num2str(file(temp:temp+1))));
                            mixed_block_flag(rowindex) = uint8(file(temp+2));
                            temp = temp + 3;
                            for region=1:2
                                table_select(rowindex,region) = uint8(bin2dec(num2str(file(temp:temp+4))));
                                temp=temp+5;
                            end
                            
                            for windows=1:3
                                subblock_gain(rowindex,windows) = uint8(bin2dec(num2str(file(temp:temp+2))));
                                temp=temp+3;
                            end
                            
                            
                            
                            
                        else
                            for region=1:3
                                table_select(rowindex,region) = bin2dec(num2str(file(temp:temp+4)));
                                temp = temp + 5;
                            end
                            
                            region0_count(rowindex) = bin2dec(num2str(file(temp:temp+3)));
                            region1_count(rowindex) = bin2dec(num2str(file(temp+4:temp+6)));
                            temp = temp + 7;
                            
                            block_type(rowindex) = uint8(0);
                            
                        end
                        
                        preflag(rowindex) = file(temp);
                        scalefac_scale(rowindex) = file(temp+1);
                        count1table_select(rowindex) = file(temp+2);
                        temp = temp +3;
                        rowindex = rowindex + 1;
                    end
                end
                
                
                
                
                where_side_ends = temp - 1;
                
                if (main_data_begin == 0)
                    i = temp;
                    next_header = cur_header_bit + ((flb * 8) + 32 + (crc_protection * 16)) - 1;
                    
                else
                    
                    
                    
                    
                    i = cur_header_bit - (8*main_data_begin);
                    
                    
                    file(cur_header_bit:where_side_ends) = [];
                    next_header = cur_header_bit + ((flb * 8) - (32 * 8)) - 1;
                end
                
                
                
                
                
                rowindex = 1;
                
                
                
                for gr=1:2
                    for ch=1:2
                        
                        
                        if (part2_3_length == 0)
                            
                        else
                            
                            
                            main_data_end = i + part2_3_length(rowindex) - 1;
                            
                            if (block_type(rowindex) == 0 || block_type(rowindex) == 1 || block_type(rowindex) == 3)
                                part2_length(rowindex) = 11* slen1(rowindex) + 10*slen2(rowindex);
                                if ( window_switching_flag(rowindex))
                                    region0_count(rowindex) = 7;
                                    region1_count(rowindex) = 13;
                                end
                            elseif (block_type(rowindex) == 2 && mixed_block_flag(rowindex)==1)
                                part2_length(rowindex) = 17* slen1(rowindex) + 18*slen2(rowindex);
                                region0_count(rowindex) = 8;
                                region1_count(rowindex) = 0;
                            elseif (block_type(rowindex) == 2 && mixed_block_flag(rowindex)==0)
                                part2_length(rowindex) = 18* slen1(rowindex) + 18*slen2(rowindex);
                                region0_count(rowindex) = 8;
                                region1_count(rowindex) = 0;
                            end
                            if (frame_counter == 7)
                                
                                
                                
                            end
                            ib4scal = i;
                            
                            [scalefactors, save_accessed] = get_scalefactors(file(i:i+part2_length(rowindex)-1), window_switching_flag(rowindex), block_type(rowindex), mixed_block_flag(rowindex), slen1(rowindex), slen2(rowindex), scfsi_share,ch , gr, saved_scalefacs);
                            if (gr == 1)
                                if (scfsi_share(ch,1) ~= 0 || scfsi_share(ch,7) ~= 0 || scfsi_share(ch,12) ~= 0 || scfsi_share(ch,17) ~= 0)
                                    
                                    
                                    saved_scalefacs(:,ch) = scalefactors(:,1);
                                end
                            end
                            
                            
                            
                            if (multi_if(save_accessed)==1 )
                                if (rowindex == 3)
                                    accessed = 1;
                                end
                                for bands=1:11
                                    if (~save_accessed(bands))
                                        i = i + double(slen1(rowindex));
                                    end
                                end
                                for bands = 12:21
                                    if (~save_accessed(bands))
                                        i = i + double(slen2(rowindex));
                                    end
                                end
                                
                            else
                                if (rowindex == 3)
                                end
                                
                                i = i + part2_length(rowindex);
                            end
                            
                            [short_cbs, long_cbs] = get_cbrange(FREQ);
                            bg_val_region(rowindex) = 2 * big_values(rowindex);
                            
                            huffman_decoded_bits(rowindex,:) = huffman_decode(file(i:end),numel(file(i:main_data_end)),bg_val_region(rowindex),region0_count(rowindex),region1_count(rowindex),table_select(rowindex,:),count1table_select(rowindex),block_type(rowindex),window_switching_flag(rowindex),long_cbs);
                            
                            i = main_data_end + 1;
                            
                            if (frame_counter == 7)
                                b4requa(rowindex,1:576) = huffman_decoded_bits(rowindex,:);
                                
                            end
                            
                            huffman_decoded_bits(rowindex,:) = requantizer(huffman_decoded_bits(rowindex,:),scalefactors,short_cbs,long_cbs ,block_type(rowindex),window_switching_flag(rowindex), global_gain(rowindex), subblock_gain(rowindex,:), preflag(rowindex), scalefac_scale(rowindex), mixed_block_flag(rowindex));
                            
                            if window_switching_flag(rowindex) && block_type(rowindex)==2
                                huffman_decoded_bits(rowindex,:) = reordering(huffman_decoded_bits(rowindex,:), short_cbs,long_cbs, mixed_block_flag(rowindex));
                            end
                            
                            scalefactors(:,:) = 0;
                            save_accessed(1,:) = 0;
                        end
                        rowindex = rowindex + 1;
                    end
                end
                
                if strcmp(mode,'joint_stereo');
                    huffman_decoded_bits = stereo_processing(huffman_decoded_bits, intensity_stereo, ms_stereo);
                end
                
                temp_PCM_samples     = IMDCT(huffman_decoded_bits , block_type, window_switching_flag,mixed_block_flag);
                temp_PCM_samples     = temp_PCM_samples .* freq_inv;
                
                temp_PCM_samples     = Filterbank(temp_PCM_samples);
                
                
                if isempty(OUTPUT)
                    OUTPUT(1,1:1152) = [temp_PCM_samples(1,:) temp_PCM_samples(3,:)];
                    OUTPUT(2,1:1152) = [temp_PCM_samples(2,:) temp_PCM_samples(4,:)];
                else
                    length_PCM = numel(OUTPUT(1,:));
                    OUTPUT(1,length_PCM+1:length_PCM+1152) = [temp_PCM_samples(1,:) temp_PCM_samples(3,:)];
                    OUTPUT(2,length_PCM+1:length_PCM+1152) = [temp_PCM_samples(2,:) temp_PCM_samples(4,:)];
                end
                
                scfsi_share(:,:) = 0;
                huffman_decoded_bits(:,:) = 0;
                saved_scalefacs(:,:) = 0;
                frame_counter = frame_counter+1;
            end
            
            
            i = next_header;
        end
        
        
    end
    i = i+1;
end

end

