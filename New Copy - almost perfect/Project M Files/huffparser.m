function [x, y, length] = huffparser (bits,table,orilength)
    match = 0;
    i = 1;
    table_length =  numel(table(:,1));

    while (i<=table_length && ~match)
        codelength_i = table(i,3);
        if (bits(1:codelength_i) == table(i, 4:codelength_i+3))
            x = table(i,1);
            y = table(i,2);
            length = table(i,3);
            length = double(length);
            match = 1;
        else
            i = i+1;
        end
    end
end

