/*
 * MATLAB Compiler: 4.18 (R2012b)
 * Date: Sat Apr 06 18:03:25 2013
 * Arguments: "-B" "macro_default" "-t" "-L" "C" "-W" "lib:mylib" "-T"
 * "link:lib" "-h" "decodertemp.m" "libmmfile.mlib" 
 */

#ifndef __mylib_h
#define __mylib_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_mylib
#define PUBLIC_mylib_C_API __global
#else
#define PUBLIC_mylib_C_API /* No import statement needed. */
#endif

#define LIB_mylib_C_API PUBLIC_mylib_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_mylib
#define PUBLIC_mylib_C_API __declspec(dllexport)
#else
#define PUBLIC_mylib_C_API __declspec(dllimport)
#endif

#define LIB_mylib_C_API PUBLIC_mylib_C_API


#else

#define LIB_mylib_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_mylib_C_API 
#define LIB_mylib_C_API /* No special import/export declaration */
#endif

extern LIB_mylib_C_API 
bool MW_CALL_CONV mylibInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_mylib_C_API 
bool MW_CALL_CONV mylibInitialize(void);

extern LIB_mylib_C_API 
void MW_CALL_CONV mylibTerminate(void);



extern LIB_mylib_C_API 
void MW_CALL_CONV mylibPrintStackTrace(void);

extern LIB_mylib_C_API 
bool MW_CALL_CONV mlxDecodertemp(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);



extern LIB_mylib_C_API bool MW_CALL_CONV mlfDecodertemp(int nargout, mxArray** out_PCM_samples, mxArray** frequency, mxArray* file_name);

#ifdef __cplusplus
}
#endif
#endif
