function [s, q, r, t, length] = count1parser (bits,table)
    match = 0;
    i = 1;
    table_length =  numel(table(:,1));

    while (i<=table_length && ~match)
        codelength_i = table(i,5);
        if (bits(1:codelength_i) == table(i, 6:codelength_i+5))
            s = table(i,1);
            q = table(i,2);
            r = table(i,3);
            t = table(i,4);
            length = double (table(i,5));
            match = 1;
        else
            i = i+1;
        end
    end
end

