%%HUFFMAN_DECODE
%%this function receives the huffman coded regions (the 3 regions and
%%count1) and decodes them according to parameters provided. It returns a
%%sequence of decoded huffman main_data for one channel in one granule. 
%%the following simple diagram shows how bits are organized in a 3-region
%%manner. R2 might not be used depending on block type.
%%|____r0_____|_______r1________|_____r2____|
%       region1border     region2border      %

%note: destination = the end of the huffman coded data.
function huffman_decoded_bits = huffman_decode(input_stream,destination,bg_val_region,region0_count,region1_count,table_select,count1table_select,block_type,window_switching_flag,long_cbs)
    i=1; %%stream iterator
    curr_freq = 1; %%frequency line iterator

    huffman_decoded_bits(1:576) = double(0); %array initialization
    
    %if (region0_count~=0) %%if block 2, borders never change. Else, finding corresponding band border and huffman tables according to the file frequency and parameters.
    if (window_switching_flag == 1 && block_type == 2)
            [huf_tab_region0, linbits_region0, max_cl0] = huffman_tables (table_select(1)); 
            [huf_tab_region1, linbits_region1, max_cl1] = huffman_tables (table_select(2));
            region1border = 36;
            region2border = 576;                     %No region2
    else %block_type 0,1,3
            [huf_tab_region0, linbits_region0, max_cl0] = huffman_tables (table_select(1));
            [huf_tab_region1, linbits_region1, max_cl1] = huffman_tables (table_select(2));
            [huf_tab_region2, linbits_region2, max_cl2] = huffman_tables (table_select(3));
            region1border = long_cbs( region0_count+1+1);
            region2border = long_cbs (region0_count + region1_count + 2+1);
    end
    

    


   

    if (count1table_select == 0)
        [huf_table_count1, ~,max_clc1] = huffman_tables (32);
    else
        [huf_table_count1, ~,max_clc1] = huffman_tables (33);
    end

    %display(i);
    %display(numel(input_stream));
    while (curr_freq< bg_val_region && i <destination)
        if (curr_freq <= region1border )
            if (table_select(1) ~= 0)
                
                [x, y, length] = huffparser ( input_stream(i:i+max_cl0-1),huf_tab_region0);
                linbits = linbits_region0;
                
            else
                %table 0, which only contains
                %0, is used for this region.
                %Smart decode and skip to the
                %next region
                curr_freq = region1border  + 1;
                huffman_decoded_bits ( 1:region1border ) = 0; %i isn't incremented because no bits occupy when the table is 0;
                continue; %continue onto next region now that iterators move to that specific region
            end
        elseif (curr_freq > region1border && curr_freq <= region2border )
            if (table_select(2) ~= 0)
                
                [x, y, length] = huffparser ( input_stream(i:i+max_cl1-1),huf_tab_region1);
                
                linbits = linbits_region1;
            else
                %table 0, which only contains
                %0, is used for this region.
                %Smart decode and skip to the
                %next region
                curr_freq = region2border  + 1;
                huffman_decoded_bits ( region1border +1:region2border ) = 0;
                continue;
            end
        elseif (curr_freq > region2border  && curr_freq <= bg_val_region)
            if (table_select(3) ~= 0)
                %display(numel(input_stream));
                %display(i+max_cl2-1);
                [x, y, length] = huffparser ( input_stream(i:i+max_cl2-1),huf_tab_region2);
                linbits = linbits_region2;
            else
                %%table 0, which only contains
                %%0, is used for this region.
                %%Smart decode and skip to the
                %%next region
                curr_freq = bg_val_region + 1;
                huffman_decoded_bits ( region2border +1:bg_val_region) = 0;
                continue;
            end
        end
        
        
        i = i+length; 
        
        

        if (x==15 && linbits ~=0)
            x = x + bin2dec(num2str(input_stream(i:i+linbits-1)));
            i = i + linbits;
        end
        if (x~=0)
            if (input_stream(i))
                x = -x;
            end
            i = i+1;
        end
        if (y==15 && linbits ~=0)
            y = y + bin2dec(num2str(input_stream(i:i+linbits-1)));
            i = i + linbits;
        end
        if (y~=0)
            if (input_stream(i))
                y = -y;
            end
            i = i+1;
        end
        huffman_decoded_bits(curr_freq:curr_freq+1) = [x y];
        curr_freq = curr_freq + 2;
    end
    %remp = 0;
     %display(curr_freq);
    while (i<=destination && curr_freq <= 573) % 573 because count1 samples are in quadruples
       % remp = remp + 1;
        %display(i);
        %display(i+max_clc1-1);
        %display(numel(input_stream));
        %display(numel(input_stream));
        [s, q, r, t, length] = count1parser(input_stream(i:i+max_clc1-1) , huf_table_count1);
        i = i + length;

        if (s ~=0)
            if(input_stream(i))
                s = -s;
            end
            i = i + 1;
        end
        if (q ~=0)
            if(input_stream(i))
                q = -q;
            end
            i = i + 1;
        end
        if (r ~=0)
            if(input_stream(i))
                r = -r;
            end
            i = i + 1;
        end
        if (t ~=0)
            if(input_stream(i))
                t = -t;
            end
            i = i + 1;
        end
        huffman_decoded_bits(curr_freq:curr_freq+3) = [s q r t];
        curr_freq = curr_freq + 4;
        
    end
    %display(remp);
    if curr_freq > 576
        disp('Discard Excessive bits');
        huffman_decoded_bits(577:end) = [];
    end
end