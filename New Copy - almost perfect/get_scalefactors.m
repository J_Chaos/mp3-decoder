function [scalefactor save_accessed]  = get_scalefactors(i_bitstream, window_switching_flag, block_type, mixed_block_flag, slen1, slen2, scfsi_share, ch, gr, saved_scalefac)
    i=double(1);
    save_accessed(1:21) = 0;
    %display(i_bitstream);
    %display(scfsi_share);
    %display(numel(i_bitstream));
    if ( window_switching_flag && block_type== 2 )
        scalefactor(21,4) = 0; %initializing
        
        if (~mixed_block_flag) %%short only
            
            for bands = 1:6
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen1-1)));
                    i = i + slen1;
                end
            end
            
            for bands = 7:12
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen2-1)));
                    i = i + slen2;
                end
            end
            
            
            
        else
            
            
            
            for bands = 1:8 %slen1 transferred to long blocks here
                %display((i_bitstream(i:i+slen1-1)));
                scalefactor(bands,1) = uint8(bit2num(i_bitstream(i:i+slen1-1)));
                i = i + slen1;
            end
            
            for bands = 4:6 %slen1 transferred to short blocks here
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen1-1)));
                    i = i+slen1;
                end
            end
            
            for bands = 7:12 %slen2 transferred to short blocks here
                for windows = 1:3
                    scalefactor(bands,windows+1) = uint8(bit2num(i_bitstream(i:i+slen2-1)));
                    i = i + slen2;
                end
            end
            
            
        end
    else
        scalefactor(21,4) = 0; %initializing
        for bands=1:21 %%fixed # of bands
            if( scfsi_share(ch,bands) == 0 || gr == 1 )
                if (bands >=1 && bands <=11)
                    %if (frame_counter == 5)
                     %                           display(slen1);
                    %                            display(slen2);
                    %                            display (i_bitstream(i:i+10));
                     %                       end
                    scalefactor(bands,1) = uint8( bit2num(i_bitstream(i : i+slen1-1) ));
                    %display(slen1);
                    %display(i+slen1);
                    i = i + double(slen1);
                    %display(i);
                
                else 
                    scalefactor(bands,1) = uint8( bit2num (i_bitstream(i : i+slen2-1) ));
                    %display(i+slen2);
                    i = i + double(slen2);
                   % display(i);
                end
                %display (class(slen1));
                % display(double(i+13945));
                
            else
                scalefactor(bands,1) = saved_scalefac(bands,ch);
                save_accessed(bands) = 1;
                
            end
        end
        %end
        %if (gr == 1)
        %    if (scfsi_share(ch,1) ~= 0 || scfsi_share(ch,7) ~= 0 || scfsi_share(ch,12) ~= 0 || scfsi_share(ch,17) ~= 0)
        %        display(ch);
        %        saved_scalefac(21,ch) = scalefactor(21,1);
        %    end
        %end
    end
    %display(slen1)
    %display(slen2)
    %display(save_accessed);
end
    