function OUTPUT = Filterbank (INPUT)


OUTPUT(1:4,1:576) = double(0);
persistent FIFO;
if isempty (FIFO)
    FIFO = zeros(2,1024);
end

load d;
u(1:512) = double(0);
holder = zeros(1,32);

for i = 0:63
    for k = 0:31
        COSINE(i+1,k+1) = cos((16+i)*(2*k+1)*(pi/64));
    end
end


for iter = 1:4
    if mod(iter,2) == 1;
        CH = 1;
    else
        CH = 2;
    end
    
    m = 1;
    for x = 1:18
        temp = INPUT(iter,m:18:576);
        FIFO(CH, 65:1024) = FIFO(CH, 1:960);
        FIFO(CH, 1:64) = 0;
        
        for j = 1:64
            FIFO(CH, j) = sum( temp .* COSINE(j,:));
        end
        
        %building vector u from fifo_v vectors.
        for i = 0:7
            for j = 0:31
                u( i*64 + j  + 1) = FIFO(CH, i*128 + j  + 1);
                u( i*64 + 33 + j) = FIFO(CH, i*128 + 97 + j);
            end
        end
        
        %windowing
        w = u .* D;
        
        for j = 0:31
            summ = 0;
            for i = 0:15
                summ = summ + w(j + 32 * i + 1);
            end
            holder(j+1) = summ;
        end
        
        output_index = m * 32;
        OUTPUT(iter, output_index - 31:output_index ) = holder;
        m = m + 1;
    end
end
end