function bitrate = BitrateRetrieve(index)
% This function decodes the bitrate information and returns the
% corresponding bitrate


BITRATELUT = [32 40 48 56 64 80 96 112 128 160 192 224 256 320];
if (index == 0)
    bitrate = 0;
elseif (index == 15)
    bitrate = 15;
else
    bitrate = BITRATELUT(1,index);
end
    

