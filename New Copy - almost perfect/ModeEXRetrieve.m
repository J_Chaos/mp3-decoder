function modeex = ModeEXRetrieve(index)

if (index == 0)
modeex = 1;
elseif (index == 1)
    modeex = 2;
elseif (index == 2)
    modeex = 1;
else
    modeex = 3;
end